// @flow

import React, { Component } from 'react';
import './App.css';
import Header from './components/header'
import Main from './components/main'

class App extends Component  {
  render() {
    return (
      <div>
        <Header />
        <div className="container mt-5">
          <Main />
        </div>
      </div>
    );
  }
}

export default App;
