
import React from 'react';
import {
  Card, CardHeader, 
  CardFooter, CardBlock,
 } from 'reactstrap';

import GetRepo from './getrepo'

const Main = (props) => {
  return (
    <Card>
      <CardHeader>Enter github repository athor and name to get latest info</CardHeader>
      <CardBlock>
        <GetRepo></GetRepo>
      </CardBlock>
      <CardFooter>Created by : Bardia Rastin</CardFooter>
    </Card>
  );
};

export default Main;