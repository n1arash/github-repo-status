import React from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

export default class GetRepo extends React.Component {

  constructor() {
    super()
    this.state = { res: undefined }
    this.getRepositoryInfo = this.getRepositoryInfo.bind(this)
  }

  getRepositoryInfo(event) {
    event.preventDefault()
    fetch('https://api.github.com/repos/angular/angular')
    .then(response => {
      response.json().then(res => this.setState({res : res}))
    })
  }

  render() {
    return (
      <Form inline onSubmit={this.getRepositoryInfo}>
        <FormGroup>
          <Label for="username">https://github.com/</Label>
          <Input type="text" size="sm" id="username" placeholder="username" />
        </FormGroup>
        <FormGroup>
          <Label for="repo">/</Label>
          <Input type="text" size="sm" id="repo" placeholder="repo name" />
        </FormGroup>
        <Button color="primary" className="ml-3" size="sm">Grab it !</Button>
      </Form>
    );
  }

}
